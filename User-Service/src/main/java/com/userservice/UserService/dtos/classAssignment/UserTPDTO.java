package com.userservice.UserService.dtos.classAssignment;

import com.userservice.UserService.dtos.UserDTO;

import java.util.List;
import java.util.Map;

public class UserTPDTO {
    /*method 1*/
    /*private String id;
    private String name;
    private String age;
    private List<String> tps;*/

    /*method 2*/
    /*private UserDTO userDTO;
    private List<UserTP> userTPS;*/

    /*method 3*/
    private Map<UserDTO,List<UserTP>>userTp;
}
