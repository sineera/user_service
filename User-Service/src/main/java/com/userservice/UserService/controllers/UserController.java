package com.userservice.UserService.controllers;

import com.userservice.UserService.dtos.OrderDTO;
import com.userservice.UserService.dtos.UserDTO;
import com.userservice.UserService.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("api/user")
public class UserController {
    @Autowired
    private UserService userService;

    //this url, where we are going to get the response=> Localhost:8080/api/user/getAll
    @GetMapping("/getAllDummyUsers")
    public List<UserDTO> getAllUsers(){
        return Arrays.asList(
                new UserDTO("1","Sineera","24"),
                new UserDTO("2","Fathima","22")
        );
    }

    /*this method is used to return all user order data*//*
    @GetMapping("/getOrdersByUserId/{id}")
    public List<OrderDTO> getOrdersByUserId(@PathVariable final  Long id){return  userService.getOrdersByUserId(id);}

    *//*this method is used to return all the user data*//*
    @GetMapping("/getAll")
    public  List<UserDTO> getAllUsers(){
        return userService.getAllUsers();
    }*/
}
