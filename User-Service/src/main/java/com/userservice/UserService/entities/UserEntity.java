package com.userservice.UserService.entities;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "user")
public class UserEntity {
    @Id
    private Long id;
    private   String name;
    private  String age;

    public Long getId() {
        return id;
    }
    public void setId(Long id){
        this.id=id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name){
        this.name=name;
    }
    public String getAge() {
        return age;
    }
    public void setAge(String age){
        this.age=age;
    }
}
