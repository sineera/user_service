package com.userservice.UserService.services;

import com.userservice.UserService.dtos.UserDTO;
import com.userservice.UserService.repositories.UserRepository;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@Service
public class UserService {
    private  final Logger LOGGER = (Logger) LoggerFactory.getLogger(UserService.class);

    @Autowired
    private UserRepository repository;

    /*this method is used to retrieve all user data*/
    public List<UserDTO> getAllUsers(){
        List<UserDTO> users = repository.findAll()
                .stream()
                .map(userEntity -> new UserDTO(
                        userEntity.getId().toString(),
                        userEntity.getName(),
                        userEntity.getAge()
                )).collect(Collectors.toList());
        return users;
    }
}
